package com.myLoan.communicationService.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "template")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Template {
    @Id
    @Column(name="id")
    private Integer id;
    @Column(name = "sms_template")
    private String sms_template;
    @Column(name = "email_template")
    private String email_template;


}
