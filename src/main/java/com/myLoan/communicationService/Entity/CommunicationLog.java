package com.myLoan.communicationService.Entity;

import lombok.*;
import org.apache.logging.log4j.message.StringFormattedMessage;

import javax.persistence.*;
import java.sql.Timestamp;


import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "communication_log")
@Data
@Getter
public class CommunicationLog {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "comm_id")
    private Integer comm_id;
    @Column(name = "comm_type")
    private String comm_type;
    @Column(name= "user_id")
    private Integer user_id;
    @Column(name = "template_id")
    private Integer template_id;
    @Column(name = "service_used")
    private String service_used;
    @Column(name = "time_sent")
    private Timestamp time_sent;
    @Column(name = "message")
    private String message;
    @Column(name = "status")
    private boolean status;


    @Builder
    public CommunicationLog(Integer comm_id,Integer template_id, String comm_type, Integer user_id, boolean status, Timestamp time_sent, String message, String service_used) {
        this.comm_id = comm_id;
        this.comm_type = comm_type;
        this.user_id = user_id;
        this.status = status;
        this.template_id=template_id;
        this.time_sent = time_sent;
        this.message = message;
        this.service_used = service_used;
    }

    public CommunicationLog() {
    }


}