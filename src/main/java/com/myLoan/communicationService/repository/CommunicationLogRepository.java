package com.myLoan.communicationService.repository;

import com.myLoan.communicationService.Entity.CommunicationLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommunicationLogRepository extends JpaRepository<CommunicationLog, Integer> {
}
