package com.myLoan.communicationService.repository;

import com.myLoan.communicationService.Entity.Template;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TemplateRepository extends JpaRepository<Template,Integer> {

}
