package com.myLoan.communicationService.bodies;

import org.springframework.web.bind.annotation.ResponseBody;

@ResponseBody
public class ComResponseBody {
    private boolean status;

    public ComResponseBody() {
    }
    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isStatus() {
        return status;
    }
}
