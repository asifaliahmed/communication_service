package com.myLoan.communicationService.bodies;

import org.springframework.web.bind.annotation.RequestBody;

import java.util.HashMap;

public class ComRequestBody {

    private String requestType;
    private HashMap<String,String> details;

    public String getRequestType() {
        return requestType;
    }

    public HashMap<String, String> getDetails() {
        return details;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public void setDetails(HashMap<String, String> details) {
        this.details = details;
    }

    @Override
    public String toString() {
        return "ComRequestBody{" +
                "reqType='" + requestType + '\'' +
                ", details=" + details +
                '}';
    }
}
