package com.myLoan.communicationService.manager;

import com.myLoan.communicationService.Entity.Template;
import com.myLoan.communicationService.repository.TemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TemplateManager {
    @Autowired
    private TemplateRepository templateRepository;

    public Template saveTemplate(Template template){
        return templateRepository.save(template);
    }

}
