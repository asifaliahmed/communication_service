package com.myLoan.communicationService.manager;

import com.myLoan.communicationService.Entity.CommunicationLog;
import com.myLoan.communicationService.Entity.Template;
import com.myLoan.communicationService.clients.Pinpoint;
import com.myLoan.communicationService.clients.SES;
import com.myLoan.communicationService.clients.Twilio;
import com.myLoan.communicationService.exception.DetailsNotFoundException;
import com.myLoan.communicationService.repository.CommunicationLogRepository;
import com.myLoan.communicationService.repository.TemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

@Service
public class CommunicationManager {
    @Autowired
    private Pinpoint pinpoint;
    @Autowired
    private Twilio twilio;
    @Autowired
    private SES ses;
    @Autowired
    private TemplateRepository templateRepository;
    @Autowired
    private CommunicationLogRepository communicationLogRepository;

    Boolean status=false;

    Timestamp timestamp=Timestamp.valueOf(java.time.LocalDateTime.now());
    private Template templateEntity=new Template();

    private HashMap<String,Integer> templateinfo=new HashMap<>();

    {
        templateinfo.put("SUOTP",1);
        templateinfo.put("LOTP",8);
        templateinfo.put("MEMIS",2);
        templateinfo.put("MEMIF",3);
        templateinfo.put("SD",4);
        templateinfo.put("ACC",5);
        templateinfo.put("APPR",6);
        templateinfo.put("LAG",7);
    }


    public HashMap<String,String> getDetails(String userID) throws DetailsNotFoundException {
        if (userID == null)
            throw new DetailsNotFoundException();
        final String uri = "http://172.31.33.44:8080/api/user/";
        RestTemplate restTemplate = new RestTemplate();
        HashMap<String,String> isResponse = restTemplate.getForObject(uri+userID+"?serviceId=cs", HashMap.class);
        return isResponse;
    }

    public boolean sendSMS(String requestType, HashMap<String,String> details) throws DetailsNotFoundException {
        templateEntity = templateRepository.getById(templateinfo.get(requestType));
        String message="";
        String template = templateEntity.getSms_template();

        switch (requestType){
            case "SUOTP":
            case "LOTP": {
                message= String.format(template,details.get("otp"),details.get("userId"));
                status = twilio.send(details.get("mobileNumber"), message);
                CommunicationLog communicationLog = CommunicationLog.builder().comm_type("OTP").service_used("twilio").user_id(Integer.parseInt(details.get("userId")))
                        .message(message).time_sent(timestamp).status(status).template_id(templateinfo.get(requestType)).build();
                communicationLogRepository.save(communicationLog);
                return status;
            }
            case "MEMIS":
            case "SD": {
                HashMap<String,String> userDetails=getDetails(details.get("userId"));
                message= String.format(template,userDetails.get("name"),details.get("amount"),details.get("loanId"),details.get("bankAccount"));
                status=pinpoint.send(userDetails.get("mobileNumber"),message);
                break;
            }
            case "MEMIF":{
                HashMap<String,String> userDetails=getDetails(details.get("userId"));
                message= String.format(template,userDetails.get("name"),details.get("loanId"),details.get("reason"));
                status= pinpoint.send(userDetails.get("mobileNumber"),message);
                break;
            }
            case "ACC":{
                HashMap<String,String> userDetails=getDetails(details.get("userId"));
                message= String.format(template,userDetails.get("name"),details.get("loanId"));
                status= pinpoint.send(userDetails.get("mobileNumber"),message);
                break;
            }
        }

        CommunicationLog communicationLog=CommunicationLog.builder().comm_type("SMS").service_used("pinpoint")
                .message(message).time_sent(timestamp).user_id(Integer.parseInt(details.get("userId")))
                .template_id(templateinfo.get(requestType)).build();
        communicationLogRepository.save(communicationLog);

        return status;
    }

    public boolean sendEmail(String requestType, HashMap<String,String> details) throws DetailsNotFoundException {

        templateEntity = templateRepository.getById(templateinfo.get(requestType));
        String template = templateEntity.getEmail_template();
        String[] emailTemplate=template.split("\\?");
        String subject=emailTemplate[0];
        String bodyTemplate=emailTemplate[1];

        String body="";

        HashMap<String,String> userDetails=getDetails(details.get("userId"));

        switch (requestType){
            case "MEMIS":
            case "SD": {
                body=String.format(bodyTemplate,userDetails.get("name"),details.get("amount"),details.get("loanId"),details.get("bankAccount"));
                break;
            }
            case "MEMIF":{
                body=String.format(bodyTemplate,userDetails.get("name"),details.get("loanId"),details.get("reason"));
                break;
            }
            case "APPR":{
                body=String.format(bodyTemplate,userDetails.get("name"),details.get("reason"));
                break;
            }
            case "ACC":{
                body=String.format(bodyTemplate,userDetails.get("name"),details.get("loanId"));
                break;
            }
            case "LAG":{
                body=String.format(bodyTemplate,userDetails.get("name"),details.get("partnerName"),details.get("loanId"),details.get("amount"),details.get("BAN"),details.get("principal"),details.get("interest"),details.get("emi"));
                break;

            }
        }

        status=ses.send(userDetails.get("emailId"),body,subject);
        CommunicationLog communicationLog=CommunicationLog.builder().comm_type("EMAIL").service_used("SES")
                .message(subject+"\n"+body).time_sent(timestamp).user_id(Integer.parseInt(details.get("userId")))
                .status(true).template_id(templateinfo.get(requestType)).build();
        communicationLogRepository.save(communicationLog);
        return status;
    }

}
