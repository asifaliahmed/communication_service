package com.myLoan.communicationService.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.net.ConnectException;

@ControllerAdvice
public class CustomizedExceptionHandling extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value = RequestTypeNotFoundException.class)
    public ResponseEntity handleRequestTypeNotFoundException(RequestTypeNotFoundException requestTypeNotFoundException) {
        return new ResponseEntity<>("Request type not found", HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = DetailsNotFoundException.class)
    public ResponseEntity handleDetailsNotFoundException(DetailsNotFoundException detailsNotFoundException) {
        return new ResponseEntity<>("Details not found for the userID sent", HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = ConnectException.class)
    public ResponseEntity handleConnectException(ConnectException connectException) {
        return new ResponseEntity<>("Connection error ", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = HttpServerErrorException.class)
    public ResponseEntity handleHttpServerError(HttpServerErrorException httpServerErrorException) {
        return new ResponseEntity<>("Incoming response error", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = HttpClientErrorException.class)
    public ResponseEntity handleHttpClientErrorException(HttpClientErrorException httpClientErrorException) {
        return new ResponseEntity<>("Bad Response from client:missing user details", HttpStatus.INTERNAL_SERVER_ERROR);
    }
    @ExceptionHandler(value = NullPointerException.class)
    public ResponseEntity handleNullPointerException(NullPointerException nullPointerException) {
        return new ResponseEntity<>("Null error", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = IllegalArgumentException.class)
    public ResponseEntity handleIllegalArgumentException(IllegalArgumentException illegalArgumentException){
        return new ResponseEntity<>("Error in arguments passed", HttpStatus.INTERNAL_SERVER_ERROR);
    }

}