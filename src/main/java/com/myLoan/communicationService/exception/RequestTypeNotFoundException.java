package com.myLoan.communicationService.exception;


public class RequestTypeNotFoundException extends RuntimeException {

    public RequestTypeNotFoundException(String message) {
        super(message);
    }

    public RequestTypeNotFoundException() {
    }


}