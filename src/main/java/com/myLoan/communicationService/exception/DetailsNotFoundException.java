package com.myLoan.communicationService.exception;

public class DetailsNotFoundException extends Exception {

    public DetailsNotFoundException() {
    }

    public DetailsNotFoundException(String message) {
        super(message);
    }
}
