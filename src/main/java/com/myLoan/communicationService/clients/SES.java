package com.myLoan.communicationService.clients;

import org.springframework.stereotype.Component;

@Component
public class SES {
    public boolean send(String email, String body, String subject) {
        System.out.println("Email is sent to:"+ email);
        System.out.println("Subject: "+subject);
        System.out.println("Body: "+body);
        return true;
    }
}
