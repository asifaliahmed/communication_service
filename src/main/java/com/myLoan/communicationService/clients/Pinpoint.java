package com.myLoan.communicationService.clients;

import org.springframework.stereotype.Component;

@Component
public class Pinpoint {
    public boolean send(String phoneNum,String message) {
        System.out.println(message +" sent to "+phoneNum);
        return true;
    }
}
