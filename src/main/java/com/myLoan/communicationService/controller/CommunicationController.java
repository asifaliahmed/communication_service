package com.myLoan.communicationService.controller;

import com.myLoan.communicationService.bodies.ComRequestBody;
import com.myLoan.communicationService.bodies.ComResponseBody;
import com.myLoan.communicationService.exception.DetailsNotFoundException;
import com.myLoan.communicationService.manager.CommunicationManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class CommunicationController {
    @Autowired
    private CommunicationManager communicationManager;

    private ComResponseBody comResponseBody=new ComResponseBody();

    @PostMapping("/sendSMS")
    public ComResponseBody sendSMS(@RequestBody ComRequestBody comRequestBody) throws DetailsNotFoundException {
        if (comRequestBody.getRequestType()==null)
            throw new NullPointerException("Request type invalid");
        boolean result=communicationManager.sendSMS(comRequestBody.getRequestType(),comRequestBody.getDetails());
        comResponseBody.setStatus(result);
        return comResponseBody;
    }
    @PostMapping("/sendEmail")
    public ComResponseBody sendEmail(@RequestBody ComRequestBody comRequestBody) throws DetailsNotFoundException {
        if (comRequestBody.getRequestType()==null)
            throw new NullPointerException("Request type invalid");
        comResponseBody.setStatus(communicationManager.sendEmail(comRequestBody.getRequestType(),comRequestBody.getDetails()));
        return comResponseBody;
    }
}
