package com.myLoan.communicationService.controller;

import com.myLoan.communicationService.Entity.Template;
import com.myLoan.communicationService.manager.TemplateManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TemplateController {

    @Autowired
    private TemplateManager templateManager;

    @PostMapping("/addtemplate")
    public Template saveTemplate(@RequestBody Template template){
        return templateManager.saveTemplate(template);
    }
}
